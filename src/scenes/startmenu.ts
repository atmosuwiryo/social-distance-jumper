import { SCENES } from "./sceneHandler";
import { Plugins, AppState } from "@capacitor/core";

const { App } = Plugins;

let gameProperties;

export class startMenuScene extends Phaser.Scene {
    constructor() {
        super({
            key: SCENES.STARTMENU
        });
    }

    init(sceneData) {
        gameProperties = sceneData;
    }

    create() {
        //click to start
        gameProperties.buttons.playButton.on("pointerdown", () => {
            this.sound.add('startSound').play({volume: 0.2});

            //get rid of start menu w/out losing other preloaded assets
            gameProperties.background.screenDarken.setVisible(false);

            gameProperties.background.title.destroy();
            gameProperties.background.copyright.destroy();

            gameProperties.buttons.playButton.destroy();

            //launch get ready scene
            this.scene.launch(SCENES.GETREADY, gameProperties);
        });

        this.sound.add('music', {loop: true}).play({volume: 0.7});

        // App state listener, on foreground or background
        // Used to mute sound that managed by howlerjs
        App.addListener('appStateChange', (state: AppState) => {
            if (state.isActive) {
                // If app is on foreground
                if (!gameProperties.muted) {
                    // If mute button is unmuted
                    this.sound.mute = false;
                }
            } else {
                this.sound.mute = true;
            }
        });
    }
};
