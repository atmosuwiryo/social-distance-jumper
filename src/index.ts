import "phaser";
import { Plugins } from '@capacitor/core';
const { App, AdMob } = Plugins;

import { preloadScene } from "./scenes/preload";
import { startMenuScene } from "./scenes/startmenu";
import { getReadyScene } from "./scenes/getready";
import { gameScene } from "./scenes/game";
import { pausedScene } from "./scenes/paused";
import { gameOverScene } from "./scenes/gameover";


// AdMob.initialize({requestTrackingAuthorization:true, testingDevices:['0123456789ABCDEF']});
AdMob.initialize();

// App backButton listener, to exit app
App.addListener('backButton', () => {
    App.exitApp();
});

//smart banner ad height reference: https://developers.google.com/admob/android/banner/smart
let adHeightInDP = 32;
if (screen.height > 400) adHeightInDP = 50;
if (screen.height > 720) adHeightInDP = 90;

//ad height in real device pixel
let adHeightReal = adHeightInDP * innerHeight/screen.height;

//the game should always be vertical or square
let width = returnLowest(
  returnLowest(screenBuffer(innerWidth), screenBuffer(innerHeight)),
  //max width is 727
  (727 * devicePixelRatio)
);
//max height is 1293 + 50 for ad space
let height = returnLowest(
  screenBuffer(innerHeight - adHeightReal),
  ((1293 + 50) * devicePixelRatio)
);

// console.log("width: ", width);
// console.log("height: ", height);
// console.log("ad height: ", adHeightInDP);

let game = new Phaser.Game({
  type: Phaser.AUTO,
  //the game should always be vertical or square
  width: width,
  //max height is 1293 + 50 for ad space
  height: height,
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: (window.innerHeight * 2 ) },
      debug: false
    }
  },
  scene: [
    preloadScene,
    startMenuScene,
    getReadyScene,
    gameScene,
    pausedScene,
    gameOverScene
  ],
  render: { pixelArt: true}
});


// AdMob.addListener('onAdSize', (info: any) => {
//   const adHeight = parseInt(info.height, 10);
//   if (adHeight > 0) {
//     // Select canvas element, resize based on ads height
//     console.log("ad height: ", adHeight);
//     const app: HTMLCanvasElement = document.querySelector('canvas');
//     app.style.height = (height - (adHeight * height/screen.height)) + 'px';
//     app.style.width = (width) + 'px';
//     // refresh must be called to sync game with new size
//     game.scale.refresh();
//   }
// });

function returnLowest(a, b) {
  if (a < b) {
    return a;
  } else {
    return b;
  }
}

function screenBuffer(windowSize) {
  return (windowSize * 1);
}

